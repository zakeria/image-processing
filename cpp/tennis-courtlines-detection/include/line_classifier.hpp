#pragma once

#include "utils.hpp"

#include <opencv2/imgproc.hpp>

#include <cassert>
#include <fstream>
#include <string>
#include <utility>

namespace he
{
template <typename L>
// require/constrain L is of type Line<T,U>
class LineClassifier 
{
    public:
      void writeCSV(const cv::Mat& img, const std::vector<L>& input,
                    const std::vector<std::string>& labels,
                    const std::string& filename);

    public:
      std::vector<std::string> createExpectedLabels();

      template <typename It>
      void advance(size_t n, It& it);

      std::pair<std::vector<L>, std::vector<std::string>>
      classifyLines(const std::vector<L>& input);
};

// LineClassifier implementation //

template<typename L>
std::vector<std::string> LineClassifier<L>::createExpectedLabels()
{
    // If you sort the correctly clustered tennis court lines by their rho
    // values, then the expected output for the tennis court is in the following
    // order:
    std::vector<std::string> expectedLabels;

    expectedLabels.push_back("Baseline");
    expectedLabels.push_back("Service Line");
    expectedLabels.push_back("Singles Sideline");
    expectedLabels.push_back("Doubles Sideline");
    expectedLabels.push_back("Centre Service Line");
    expectedLabels.push_back("Singles Sideline");
    expectedLabels.push_back("Doubles Sideline");

    return expectedLabels;
}

template <typename L>
template <typename It>
void LineClassifier<L>::advance(size_t n, It& it)
{
    for (int i = 0; i < n; i++)
        it++;
}

template <typename L>
std::pair<std::vector<L>, std::vector<std::string>>
LineClassifier<L>::classifyLines(const std::vector<L>& input)
{
    auto expectedLabels = createExpectedLabels();

    std::vector<L> res(input.begin(), input.end());

    // Sort lines by their rho
    std::sort(std::begin(res), std::end(res), [](const L& l1, const L& l2) 
    {
        return l1.getRho() > l2.getRho();
    });

    // Map extracted lines to their labels
    for (int i = 0; i < input.size(); i++)
    {
        auto& line = input.at(i);
        auto& label = expectedLabels.at(i);

        res.at(i).setName(label);
    }

    return std::make_pair(res, expectedLabels);
}

template <typename L>
void LineClassifier<L>::writeCSV(const cv::Mat& img,
                                 const std::vector<L>& input,
                                 const std::vector<std::string>& labels,
                                 const std::string& filename)
{
    std::ofstream os(filename);

    if (!os.is_open())
    {
        std::cerr << "Error opening file."
                  << "\n";
        return;
    }

    assert((labels.size() == input.size()) &&
           "Error - The number of clusters do not match the number of labels.");

    os << "Line name, Intervals"
       << "\n";

    // Write points from left to right
    for (int i = 0; i < input.size(); i++)
    {
        auto& line = input.at(i);
        auto& label = labels.at(i);

        cv::LineIterator it(img, line.x1y1, line.x2y2, 8, true);

        // Split lines into intervals
        auto delta = it.count / 5;

        os << label << ",";
        for (int j = 0; j < it.count; j += 2 * delta)
        {
            os << "[";
            for (int k = 0; k < delta; k++)
            {
                cv::Point pt = it.pos();
                os << "(" << pt.x << "  " << pt.y << ")";

                if (k == delta - 1)
                    os << "]";

                os << ",";
                it++;
            }
            advance(delta, it);
        }
        os << "\n";
    }
}
} // namespace he

