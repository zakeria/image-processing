#pragma once

#include <opencv2/core/types.hpp>

#include <string>

namespace he
{
template <typename T, typename U>
class Line
{
  private:
    T rho;
    U theta;

    std::string name;

  public:
    // TODO get/set
    cv::Point x1y1;
    cv::Point x2y2;

  public:
    Line(T rho_, U theta_, const cv::Point& x1y1_,
         const cv::Point& x2y2_);

    Line(T rho_, U theta_, const cv::Point& x1y1_, const cv::Point& x2y2_,
         const std::string& name_);

    Line(const Line& other) = default;
    Line& operator=(Line&& other) = default;

  public:
    const T getRho() const;
    const U getTheta() const;

    const std::string& getName() const;

    void setName(const std::string& name);
    void setRho(T rho);
    void setTheta(T rho);
};

// Line implementation //
template <typename T, typename U>
Line<T, U>::Line(T rho_, U theta_, const cv::Point& x1y1_,
                 const cv::Point& x2y2_)
    : rho(rho_), theta(theta_), x1y1(x1y1_), x2y2(x2y2_)
{
}

template <typename T, typename U>
Line<T, U>::Line(T rho_, U theta_, const cv::Point& x1y1_,
                 const cv::Point& x2y2_, const std::string& name_)
    : rho(rho_), theta(theta_), x1y1(x1y1_), x2y2(x2y2_), name(name_)
{
}

template <typename T, typename U>
const T Line<T, U>::getRho() const
{
    return rho;
}

template <typename T, typename U>
const U Line<T, U>::getTheta() const
{
    return theta;
}

template <typename T, typename U>
const std::string& Line<T, U>::getName() const
{
    return name;
}

template <typename T, typename U>
void Line<T, U>::setName(const std::string& name) 
{
    this->name = name;
}
} // namespace he
