#pragma once

#include <string>

namespace he
{

template <typename T, typename U>
Line<T, U> createLine(T rho, U theta, std::string);

template <typename T, typename U>
Line<T, U> createLine(T rho, U theta);


// Implementation
template <typename T, typename U>
Line<T, U> createLine(T rho, U theta)
{
    auto a = cos(theta);
    auto b = sin(theta);

    auto x0 = a * rho;
    auto y0 = b * rho;

    cv::Point pt1;
    cv::Point pt2;

    pt1.x = cvRound(x0 + 1000 * (-b));
    pt1.y = cvRound(y0 + 1000 * (a));
    pt2.x = cvRound(x0 - 1000 * (-b));
    pt2.y = cvRound(y0 - 1000 * (a));

    return Line<T, U>(rho, theta, pt1, pt2);
}

template <typename T, typename U>
Line<T, U> createLine(T rho, U theta, std::string name)
{
    auto a = cos(theta);
    auto b = sin(theta);

    auto x0 = a * rho;
    auto y0 = b * rho;

    cv::Point pt1;
    cv::Point pt2;

    pt1.x = cvRound(x0 + 1000 * (-b));
    pt1.y = cvRound(y0 + 1000 * (a));
    pt2.x = cvRound(x0 - 1000 * (-b));
    pt2.y = cvRound(y0 - 1000 * (a));

    return Line<T, U>(rho, theta, pt1, pt2, name);
}

} // namespace he
