#pragma once

#include <opencv2/core/fast_math.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>

#include "line.hpp"
#include "utils.hpp"

#include <algorithm>
#include <array>
#include <cmath>
#include <iostream>
#include <random>
#include <tuple>
#include <vector>

#include <xtensor/xarray.hpp>
#include <xtensor/xadapt.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xview.hpp>

namespace he
{
template <typename L>
// require/constrain L is of type Line<T,U>
class LineExtractor
{
  private:
    // kSize, sigmaX
    std::tuple<int, double> paramsBlur  = {15, 0};
    // Lo, Hi, kSize
    std::tuple<double, double, int> paramsCanny = {50, 150, 3};
    // rho, theta, thresh
    std::tuple<double, double, double> paramsHoughes = {1, 180, 130};

  public:
    LineExtractor();

  public:
    std::vector<L> extractLines(const cv::Mat& input);
    std::vector<L> findLines(const cv::Mat& input);

    cv::Mat blurImage(const cv::Mat& input);
    cv::Mat detectEdges(const cv::Mat& input);

    std::vector<L> sampleLines(const std::vector<L>& input, int nSamples);

    std::vector<L> filterClusters(const std::vector<L>& input, double thresh);
    std::vector<L> clusterLines(const std::vector<L>& input, int nClusters, double thresh);

  public:
    const std::tuple<int, double> getParamsBlur() const;
    const std::tuple<double, double, double> getParamsHoughes() const;

    void setParamsCanny(const std::tuple<int, double>& paramsCanny);
    void setParamsHough(const std::tuple<double, double, double>& paramsHough);
};

// LineExtractor implementation // 
template <typename L>
LineExtractor<L>::LineExtractor() {}

/* Apply Gaussian blur to blob artefacts/net */
template <typename L>
std::vector<L> LineExtractor<L>::extractLines(const cv::Mat& input)
{
    cv::Mat edgeMap;
    edgeMap = blurImage(input);
    /* // Detect edges using Canny edge detector */
    edgeMap = detectEdges(edgeMap);

    std::vector<L> res = findLines(edgeMap);

    return res;
}

template <typename L>
cv::Mat LineExtractor<L>::blurImage(const cv::Mat& input)
{
    cv::Mat res;

    int kSize = std::get<0>(paramsBlur);
    double sigmaX = std::get<1>(paramsBlur);

    cv::GaussianBlur(input, res, cv::Size(kSize, kSize), sigmaX);
    return res;
}

template <typename L>
cv::Mat LineExtractor<L>::detectEdges(const cv::Mat& input)
{
    cv::Mat res;

    double thresh1 = std::get<0>(paramsCanny);
    double thresh2 = std::get<1>(paramsCanny);
    int kSize = std::get<2>(paramsCanny);

    cv::Canny(input, res, thresh1, thresh2, kSize);

    return res;
}

template <typename L>
std::vector<L> LineExtractor<L>::findLines(const cv::Mat& input)
{
    std::vector<L> res;

    double rhoHough = std::get<0>(paramsHoughes);
    double thetaHough = std::get<1>(paramsHoughes);
    double threshHough = std::get<2>(paramsHoughes);

    std::vector<cv::Vec2f> lines;
    cv::HoughLines(input, lines, rhoHough, (CV_PI) / thetaHough, threshHough,0,0);

    for (auto& line : lines)
    {
        double rho = line[0];
        double theta = line[1];

        res.push_back(createLine(rho, theta));
    }
    return res;
}

template <typename L>
std::vector<L> LineExtractor<L>::sampleLines(const std::vector<L>& input,
                                             int nSamples)
{
    std::vector<L> sampleLines;
    std::sample( 
        input.begin(),
        input.end(), 
        std::back_inserter(sampleLines),
        nSamples, 
        std::mt19937{std::random_device{}()}
    );

    return sampleLines;
}

// Once the data is clustered, removing nearby lines is simple
// for each cluster center, remove close points for the given threshold value
template <typename L>
std::vector<L> LineExtractor<L>::filterClusters(const std::vector<L>& input,
                                                double thresh)
{
    std::vector<L> resCopy(input.begin(), input.end());
    std::set<std::string> checked;

    for (auto& rLine : input)
    {
        if (checked.count(rLine.getName()))
            continue;

        checked.insert(rLine.getName());

        resCopy.erase(std::remove_if(
                          resCopy.begin(), resCopy.end(),
                          [&rLine, &checked, thresh](const L& l) 
                          {
                              auto dist = std::sqrt(
                                  std::pow(rLine.getRho() - l.getRho(), 2) +
                                  std::pow(rLine.getTheta() - l.getTheta(), 2));

                              if ((dist < thresh) &&
                                  (rLine.getName() != l.getName()))
                              {
                                  checked.insert(l.getName());
                                  return true;
                              }
                              return false;
                          }),
                      resCopy.end());
    }
    return resCopy;
}

template <typename L>
std::vector<L> LineExtractor<L>::clusterLines(const std::vector<L>& lines,
                                              int nClusters, double thresh)
{
    // Initialise clusters
    xt::xarray<double> clusters = xt::zeros<double>({nClusters, 2});
    std::vector<L> lineSamples = sampleLines(lines, nClusters);
    for (int k = 0; k < nClusters; k++)
    {
        clusters(k, 0) = lineSamples[k].getRho();
        clusters(k, 1) = lineSamples[k].getTheta();
    }

    const int nLines = lines.size();
    xt::xarray<double> linesLabeled = xt::zeros<double>({1, nLines});

    for (;;)
    {
        xt::xarray<double> prevClusters(clusters.shape());
        std::copy(clusters.cbegin(), clusters.cend(), prevClusters.begin());

        for (int i = 0; i < lines.size(); i++)
        {
            auto rho = lines[i].getRho();
            auto theta = lines[i].getTheta();

            auto dist =
                xt::sqrt(xt::pow(rho - xt::view(clusters, xt::all(), 0), 2) +
                         xt::pow(theta - xt::view(clusters, xt::all(), 1), 2));

            xt::xarray<double> minDist =
                xt::from_indices(xt::where(xt::equal(dist, xt::amin(dist))));

            linesLabeled(i) = minDist(0);
        }

        // Update centroids 
        for (int i = 0; i < nClusters; i++)
        {
            xt::xarray<size_t> idx =
                xt::from_indices(xt::where(xt::equal(linesLabeled, i)));

            int nLinesCluster = idx.shape(1);

            double rhoSum = 0;
            double thetaSum = 0;

            for (int j = 0; j < nLinesCluster; j++)
            {
                Line line = lines[idx(1, j)];

                rhoSum += line.getRho();
                thetaSum += line.getTheta();
            }

            if (nLinesCluster > 0)
            {
                clusters(i, 0) = rhoSum / nLinesCluster;
                clusters(i, 1) = thetaSum / nLinesCluster;
            }
        }

        // Converge when cluster centers stop changing
        bool converged = (prevClusters == clusters);
        if(converged)
        {
            break;
        }
    }

    // Convert xt::tensor back to std::vector
    std::vector<L> res;
    for (int i = 0; i < nClusters; i++)
    {
        auto rho = clusters(i, 0);
        auto theta = clusters(i, 1);

        std::string name = "l";
        name += std::to_string(i);

        res.push_back(createLine(rho, theta, name));
    }

    // Post-process clusters
    return filterClusters(std::move(res), thresh);
}

} // namespace he
