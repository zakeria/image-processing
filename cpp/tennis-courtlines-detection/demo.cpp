#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <utility>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/types_c.h>

#include "line.hpp"
#include "line_extractor.hpp"
#include "line_classifier.hpp"

using namespace cv;

bool readRawImgToMat(const std::string& filename, cv::Mat& out, int rows,
                   int cols)
{
    auto buffer = std::make_unique<uint8_t[]>(rows * cols);

    FILE* fp = fopen(filename.c_str(), "rb");
    if (!fp)
    {
        std::cerr << "Error reading file."
                  << "\n";
        return false;
    }

    fread(buffer.get(), rows * cols, 1, fp);
    fclose(fp);

    out = Mat(cols, rows, CV_MAKETYPE(cv::DataType<uint8_t>::type, 1));
    memcpy(out.data, buffer.get(), rows * cols * sizeof(uint8_t));

    return true;
}

void demoExtractLines(const cv::Mat& input)
{
    Mat extractedLines;
    cv::cvtColor(input, extractedLines, CV_GRAY2RGB);

    he::LineExtractor<he::Line<double, double>> le;
    he::LineClassifier<he::Line<double, double>> lc;

    auto lines = le.extractLines(input);

    for (auto line : lines)
    {
        cv::line(extractedLines, line.x1y1, line.x2y2, cv::Scalar(0, 0, 255), 2,
                 cv::LINE_AA);
    }
    imshow("Extracted lines", extractedLines);
    waitKey(0);
}

void demoClusterLines(const cv::Mat& input)
{
    // Number of cluster centers
    int nClusters = 21;
    // Threshold for post-process of Kmeans
    int threshClusters = 30;

    Mat linesClustered;
    cvtColor(input, linesClustered, CV_GRAY2RGB);

    he::LineExtractor<he::Line<double, double>> le;

    auto lines = le.extractLines(input);
    auto lineClusters = le.clusterLines(lines, nClusters, threshClusters);

    for (auto line : lineClusters)
    {
        cv::line(linesClustered, line.x1y1, line.x2y2, cv::Scalar(0, 0, 255), 2,
                 cv::LINE_AA);
    }
    imshow("Clustered lines", linesClustered);
    waitKey(0);
}

void demoClassifyLines(const cv::Mat& input)
{
    // Number of cluster centers
    int nClusters = 21;
    // Threshold for post-process of Kmeans
    int threshClusters = 30; 

    Mat linesClassified;
    cvtColor(input, linesClassified, CV_GRAY2RGB);

    he::LineExtractor<he::Line<double, double>> le;
    he::LineClassifier<he::Line<double, double>> lc;

    auto lines = le.extractLines(input);
    auto lineClusters = le.clusterLines(lines, nClusters, threshClusters);

    // Result contains the classified lines, where result.at(i).name should
    // match labels.at(i)
    auto [result, labels] = lc.classifyLines(lineClusters);

    for (int i = 0; i < result.size(); i++)
    {
        assert((result.at(i).getName() == labels.at(i)) &&
               "Error - classification is wrong.");
    }
    // Write the result as a CSV file
    lc.writeCSV(linesClassified, result, labels, "lines.csv");
}

int main()
{
    int rows = 1392;
    int cols = 550;

    cv::Mat mat;
    bool readResult = readRawImgToMat("image.raw", mat, rows, cols);

    if (!readResult)
        return EXIT_FAILURE;

    // Demos //
    demoExtractLines(mat);
    demoClusterLines(mat);
    demoClassifyLines(mat);

    return 0;
}

