# Image Processing

A repository containing implementations of various Digital Image Processing algorithms such as edge detection, image denoising and segmentation.

Written in C++ and MATLAB.