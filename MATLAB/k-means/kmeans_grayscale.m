function [L, clusters] = my_kmeans_greyscale(I, k)

I = im2double(I);

[rows, cols] = size(I);
L = zeros(rows,cols);

% Initialise cluster centers
clusters = zeros(k,1);
clusters(1) = 0;
clusters(2) = 1;

while(true)   
    for i = 1:rows
        for j = 1:cols
            distance = abs(I(i,j) - clusters);
            
            % Get the index of the closest cluster
            cluster_index = find(distance == min(distance));            
            L(i,j) = cluster_index;
        end
    end
    
    % Update cluster centers
    prev_clusters = clusters;
    for i=1:k
        [row, col] = find(L == k);
        
        sum = 0; 
        for j=1:length(row)
            pixel = I(row(j), col(j));
            sum = sum + pixel;
        end
        average_pixel = sum/length(row); % normalise
        clusters(k) = average_pixel;
    end
    
    if(clusters == prev_clusters)
        break;
    end    
end
end