function E = my_conv2(I,K)

[X Y] = size(I);
[SX SY] = size(K);

E = zeros(X,Y); % Output 

ox = ceil(SX / 2); 
oy = ceil(SY / 2);

% Loop over all the pixels in I
for x = 1:X
    for y = 1:Y
        S = 0;
        % Apply the filter centred on (x,y)
        for i = 1:SX
            for j = 1:SY
                S = S + K(i,j)*ImageEntryC(I, x-i+ox, y-j+oy); 
            end 
        end
        E(x,y) = S;
    end
end


% acts like padding...
function v = ImageEntryC(I, i, j)
% This function simply returns the value of the image at a specified
% i,j position, or a zero if i,j is out of bounds
[X Y] = size(I);

if( i<=0 || j <=0 || i>X || j>Y)
    v = 0;
else
    v = I(i,j);
end