function [result] = naiveNonLocalMeans(I, sigma, h, patchSize, windowSize)

[X,Y] = size(I);
result = zeros(X,Y);

for curRow=1:X
    for curCol=1:Y
        % Compute our naive template matching
        [offsetsRows, offsetsCols, distances] = templateMatchingNaive(I, curRow, curCol, patchSize, windowSize);               
       
        offsetCount = size(offsetsRows);
        
        weightedAvg = 0; 
        sumOfWeights = 0;
        for offset_idx=1:offsetCount
            % Offset of each patch
            offsetRow = offsetsRows(offset_idx);
            offsetCol = offsetsCols(offset_idx);  
            
            % Ssd of each patch
            patchSqDiff = distances(offset_idx); 

            % Compute the weight
            weight = computeWeighting(patchSqDiff, h, sigma, patchSize);

            % Get center pixel of each patch
            pixel = pixAt(I, curRow + offsetRow, curCol + offsetCol); 
            % Weigh the noisy pixel and the patch
            weightedAvg = weightedAvg + (pixel * weight);
            % Sum the weight between the patches - C(p)
            sumOfWeights = sumOfWeights + weight; 
        end 
        % Store the result in the output image
        result(curRow,curCol) = weightedAvg/sumOfWeights;
        
    end
end

end