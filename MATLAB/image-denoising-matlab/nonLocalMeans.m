function [result] = nonLocalMeans(I,integral_images, sigma, h, patchSize, windowSize)

[X,Y] = size(I);
result = zeros(X,Y);

% This routine will be identical to the naiveLocalMeans.m, where the only
% difference is the approach for calculating our distance metric - templateMatchingIntegralImage.m.
for x=1:X
    for y=1:Y
        % Compute our integral template matching
        [offsetsRows, offsetsCols, distances] = templateMatchingIntegralImage(I,integral_images, x, y, patchSize, windowSize); 
        
        offsetCount = size(offsetsRows);
        
        weightedAvg = 0; 
        sumOfWeights = 0;
        for offsetIdx=1:offsetCount
            % Offset of each patch
            offsetRow = offsetsRows(offsetIdx);
            offsetCol = offsetsCols(offsetIdx);  
            
            % Ssd of each patch
            patchSqDiff = distances(offsetIdx);    
            
            % Compute the weight
            weight = computeWeighting(patchSqDiff, h, sigma, patchSize);

            % Get center pixel of each patch
            pixel = pixAt(I, x + offsetRow, y + offsetCol); 
            % Weigh the noisy pixel and the patch
            weightedAvg = weightedAvg + (pixel * weight);
            % Sum the weight between the patches - C(p)
            sumOfWeights = sumOfWeights + weight; 
        end
        % Store the result in the output image       
        result(x,y) = weightedAvg / sumOfWeights;
    end
end
end