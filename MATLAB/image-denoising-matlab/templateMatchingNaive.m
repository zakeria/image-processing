function [offsetsRows, offsetsCols, distances] = templateMatchingNaive(I, row, col, patchSize, searchWindowSize)

winArea = searchWindowSize^2;

offsetsRows = zeros(winArea,1); 
offsetsCols = zeros(winArea,1);
distances   = zeros(winArea,1);

winWidth = floor(searchWindowSize/2);
winHeight = winWidth;

patchWidth = floor(patchSize/2);
patchHeight = patchWidth;

idx = 1;
for curRow = row - winWidth:row + winWidth
    for curCol = col - winHeight:col + winHeight
        sumSqDiff = 0;
        for patchRow = -patchWidth:patchWidth
            for patchCol = -patchHeight:patchHeight      
                % Compute the patch distance to our pixel
                dist = pixAt(I, curRow + patchRow, curCol + patchCol) ...
                     - pixAt(I, row + patchRow, col + patchCol);
                sumSqDiff = sumSqDiff + dist^2;
            end
        end
        distances(idx) = sumSqDiff;
        % Set the the patch offsets from the pixAt(row,col)
        offsetsRows(idx) = curRow - row;
        offsetsCols(idx) = curCol - col;
        idx = idx + 1;
    end
end
end