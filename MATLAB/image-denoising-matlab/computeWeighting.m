function [result] = computeWeighting(d, h, sigma, patchSize)
    %Implement weighting function from the slides
    %Be careful to normalise/scale correctly!
    
    %REPLACE THIS
    
    % Our pixel is centered in our kernel, with patch size of (2r)^2.
    % This appears to give us the result we are looking for.
    % ref http://www.numerical-tours.com/matlab/denoisingadv_6_nl_means/
    d = d/patchSize^2;
    
    % Compute the weighing between the two patches.
    result = exp(-(max(d-2*sigma^2,0) / h^2));
end