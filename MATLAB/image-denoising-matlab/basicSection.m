%% Some parameters to set - make sure that your code works at image borders!

% Row and column of the pixel for which we wish to find all similar patches 
% NOTE: For this section, we pick only one patch
row = 10;
col = 10; 

% Patchsize - make sure your code works for different values
patchSize = 5; % test

% Search window size - make sure your code works for different values
searchWindowSize = 13; 


%% Implementation of work required in your basic section-------------------

% TODO - Load Image
% begin with grayscale?
image = double(rgb2gray(imread('images/debug/alleyNoisy_sigma20.png')));

% TODO - Fill out this function
%image_ii = computeIntegralImage(image);

% TODO - Template matching for naive SSD (i.e. just loop and sum)
[offsetsRows_naive, offsetsCols_naive, distances_naive] = templateMatchingNaive(image, row, col,...
    patchSize, searchWindowSize);

% Pre-processing:
% Store integral images in a cell, which will be mapped to each offset.
% we want as close to constant access as possible - and cells may provide
% hashmap like access time.
integralImages = {};
winWidth = floor(searchWindowSize/2);
winHeight = winWidth;
% We will compute an integral image for each offset <x,y>.
for x =-winWidth:winWidth
    for y =-winHeight:winHeight
        % Compute the integal image for each window offset.
        integralImage = computeIntegralImage(image, x, y, patchSize, searchWindowSize); 
        % % -winWidth+winWidth is zero at the least initially so we'll + 1 to map this to
        % positive indices.
        idxX = x + winWidth + 1;
        idxY = y + winHeight + 1;
        % Store our integral images for each offset.
        integralImages{idxX, idxY} = integralImage;
    end
end


% TODO - Template matching using integral images
[offsetsRows_ii, offsetsCols_ii, distances_ii] = templateMatchingIntegralImage(image, integralImages, row, col,...
    patchSize, searchWindowSize);


%% Let's print out your results--------------------------------------------

% NOTE: Your results for the naive and the integral image method should be
% the same!
for x=1:length(offsetsRows_naive)
    disp(['offset rows: ', num2str(offsetsRows_naive(x)), '; offset cols: ',...
        num2str(offsetsCols_naive(x)), '; Naive Distance = ', num2str(distances_naive(x),10),...
        '; Integral Im Distance = ', num2str(distances_ii(x),10)]);
end